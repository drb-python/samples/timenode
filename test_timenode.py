from timenode import TimeNode
import time

timenode = TimeNode(2)


# Attach the event listener
@timenode.changed.register
def change_manager(obj, key, value):
    if key == 'value':
        print(f"{obj} : ({key}: {value})")
    else:
        print(f"Change detected: {obj} : ({key}: {value})")


timenode.start()
time.sleep(10)
timenode.stop()
