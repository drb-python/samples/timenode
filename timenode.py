from drb.utils.logical_node import DrbLogicalNode
import datetime
import threading


class TimeNode(DrbLogicalNode):

    def __init__(self, step_s: int = 1):
        super(TimeNode, self).__init__("time_node")
        self.step_s = step_s
        self.started = False

    def timer_func(self):
        if self.started:
            self.value = datetime.datetime.now()
            threading.Timer(self.step_s, self.timer_func).start()

    def start(self):
        """Start a new timer"""
        if not self.started:
            self.started = True
            self.timer_func()

    def stop(self):
        """Stop the timer"""
        self.started = False
