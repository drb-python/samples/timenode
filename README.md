# Time Node Snippet

Sample usage of a drb node containing time.

# Node Events with DrbNode

This current sample class implements a DrbNode (aka in memory node is DrbLogicalNode) and node with recieves external informaton as time.


```python
from drb.utils.logical_node import DrbLogicalNode
import datetime
import threading


class TimeNode(DrbLogicalNode):

    def __init__(self, step_s: int = 1):
        super(TimeNode, self).__init__("time_node")
        self.step_s = step_s
        self.started = False

    def timer_func(self):
        if self.started:
            self.value = datetime.datetime.now()
            threading.Timer(self.step_s, self.timer_func).start()

    def start(self):
        """Start a new timer"""
        if not self.started:
            self.started = True
            self.timer_func()

    def stop(self):
        """Stop the timer"""
        self.started = False

```

Once the `DrbNode` created, we are able to create a new instance:


```python
timenode = TimeNode(2)
```

Drb implements registration decoration to attach event listener


```python
@timenode.changed.register
def change_manager(obj, key, value):
    if key == 'value':
        print(f"{obj} : ({key}: {value})")
    else:
        print(f"Change detected: {obj} : ({key}: {value})")
```

Now, we can play with the event manager


```python
import time
timenode.start()
time.sleep(10)
timenode.stop()
```

    <time_node>2022-02-04 19:14:50.080282</time_node> : (value: 2022-02-04 19:14:50.080282)
    <time_node>2022-02-04 19:14:52.087084</time_node> : (value: 2022-02-04 19:14:52.087084)
    <time_node>2022-02-04 19:14:54.089145</time_node> : (value: 2022-02-04 19:14:54.089145)
    <time_node>2022-02-04 19:14:56.090987</time_node> : (value: 2022-02-04 19:14:56.090987)
    <time_node>2022-02-04 19:14:58.092992</time_node> : (value: 2022-02-04 19:14:58.092992)


We can see the events are well managed by the change event implemented in Drb, which notify our callback each 2 seconds, when the node value changed.


